const { v4: uuid } = require('uuid');
const { deepClone } = require('./utils');
const { index } = require('./indexer');

function _createProfileFrom(entry) {
  return Object.assign(deepClone(entry), { id: uuid() });
}

function _createEntityFrom(entry) {
  const initialProfile = _createProfileFrom(entry);

  return {
    id: uuid(),
    entityType: initialProfile.entityType,
    profiles: [initialProfile],
  };
}

function _popEntry(idx) {
  const key = Object.keys(idx)[0];
  return _popEntryByKey(idx, key);
}

function _popEntryByKey(idx, key) {
  const entry = _getEntry(idx, key);
  delete idx[key];
  return entry;
}

function _getEntry(idx, key) {
  return idx[key];
}

function _findMatches(profiles, idx) {
  const result = [];
  const urls = profiles.map(profile => profile.linksTo).reduce((acc, item) => [...acc, ...item], []);

  urls.forEach(url => {
    if (!idx[url]) {
      return;
    }
    const entry = _getEntry(idx, url);
    const profile = _createProfileFrom(entry);
    result.push(profile);
  });
  return result;
}

function _process(idx) {
  const entry = _popEntry(idx);
  const entity = _createEntityFrom(entry);
  if (entity.profiles.length) {
    const matchingProfiles = _findMatches(entity.profiles, idx);
    entity.profiles = [...entity.profiles, ...matchingProfiles];
    matchingProfiles.forEach(profile => delete idx[profile.url]);
  }
  return entity;
}

exports.match = function match(data) {
  const idx = index(data);
  const result = [];

  while (Object.keys(idx).length) {
    const entity = _process(idx);
    result.push(entity);
  }

  console.dir(result, { depth: null, colors: true });

  return result;
};
