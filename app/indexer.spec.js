const { index } = require('./indexer');

describe('Indexer', () => {
  it('should return empty object when data is no array', () => {
    const data = 'foo';

    const result = index(data);
    expect(result).toEqual({});
  });

  it('should return empty object when no entities provided', () => {
    const data = [];

    const result = index(data);

    expect(result).toEqual({});
  });

  it(`should return an object which keys are entities' urls`, () => {
    const data = [
      { url: 'aaa', entityType: 'person' },
      { url: 'bbb', entityType: 'person' },
    ];

    const result = index(data);

    expect(Object.keys(result)).toEqual(['aaa', 'bbb']);
  });

  it(`should add only allowed entity types`, () => {
    const data = [
      { url: 'aaa', entityType: 'person' },
      { url: 'bbb', entityType: 'organization' },
      { url: 'ccc', entityType: 'uncategorized' },
      { url: 'ddd' },
    ];

    const result = index(data);

    expect(Object.keys(result)).toEqual(['aaa', 'bbb']);
  });

  it('should return an object which values are entities matching the url (the key)', () => {
    const data = [{ url: 'aaa', entityType: 'person', linksTo: ['bbb'] }];

    const result = index(data);
    const key = data[0].url;

    expect(result[key]).toEqual(data[0]);
  });

  it('should skip incomplete entities', () => {
    const data = [{ url: null }, { foo: 'aaa' }, { url: 'bbb', entityType: 'person' }];

    const result = index(data);

    expect(Object.keys(result)).toEqual(['bbb']);
  });
});
