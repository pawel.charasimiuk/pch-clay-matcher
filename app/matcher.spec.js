const { match } = require('./matcher');

describe('Matcher', () => {
  it(`should return empty array for no data`, () => {
    const data = [];

    const result = match(data);

    expect(result).toEqual([]);
  });

  it(`should return an array of profiles`, () => {
    const data = [
      {
        url: 'https://linkedin.com/aaa',
        entityType: 'person',
        linksTo: ['http://aaa.com'],
      },
    ];

    const result = match(data);

    expect(result).toEqual(jasmine.any(Array));
    expect(Object.keys(result[0])).toContain('id');
  });
});
