// TODO test
function _indexFilter(profile) {
  const ALLOWED_TYPES = ['organization', 'person'];
  return profile && ALLOWED_TYPES.includes(profile.entityType);
}

function _indexReducer(index, profile) {
  if (profile.url && !index[profile.url]) {
    index[profile.url] = profile;
  }
  return index;
}

exports.index = function index(data) {
  if (!Array.isArray(data)) return {};

  return data.filter(_indexFilter).reduce(_indexReducer, {});
};
