# Clay Interview Task

https://www.notion.so/Clay-interview-task-40adb5107a2e4404a12a79b06126822a

## Usage
```
node index.js
```

## Scripts
- *npm run lint* - runs code linter 
- *npm run prettify* - runs code formatter
- *npm run test* - runs unit tests

## Assumptions
- The form of application:  
CLI
- The form of input/output data:  
Currently hardcoded input. Outputs to the console. The plan is to use fs (file I/O). See TODO 
- JSON Validity
The input data is always valid.
- Requirement 2.3: how to distinguish a website from a profile?   
A regexp check against a whitelist of social profile domains (GitHub, LinkedIn, Facebook, Twitter), any other url is considered as a website 
- Requirement 3: ID format  
Random UUID
- Requirement 4: existing Profiles and Entities in the database
The plan is to create a Data Access Layer which would make data processing independent from their source
Aggregating input data with existing would be a part of preprocessing in DAL  
-  Profile uniqueness  
URL is the unique key.
-  Profile preprocessing  
Currently the indexer takes the first profile occurance and throws away any duplication appearing later.
From a business point of view it seems to be more useful to merge duplicated entries during preprocessing.

## FIXME 
- Index should be rechecked when new profiles appear
Considering recursive _findMatches method as a quickest solution. Then rethinking indexing to eliminate recursion. 

## TODO
- Logic: Support requirement 2.3 (websites)

- Architecture: Add DAL/DAO
- Logic: fs (file I/O) 
- Logic: command line arguments processing 
- Logic: duplicate profile merge strategies
- Logic: support existing (db) data
- Development: Cover with unit tests
- Development: Avoid side effects in entry index
- Development: Add type checking


